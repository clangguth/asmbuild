name := "asmbuild"

version := "1.0"

scalaVersion := "2.11.6"

resolvers ++= Seq(
  "shapemodelling unibas" at "http://shapemodelling.cs.unibas.ch/repository/public"
)

libraryDependencies ++= Seq(
    "ch.unibas.cs.gravis" %% "scalismo" % "develop-SNAPSHOT",
    "ch.unibas.cs.gravis" % "scalismo-native-all" % "2.1.+"
)
    