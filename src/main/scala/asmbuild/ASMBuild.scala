package asmbuild

import java.io.{File, FilenameFilter}

import scalismo.common.{RealSpace, Scalar}
import scalismo.geometry.{Point, _3D}
import scalismo.image.DiscreteScalarImage
import scalismo.io.ImageIO.ScalarType
import scalismo.io.{ActiveShapeModelIO, ImageIO, MeshIO, StatismoIO}
import scalismo.mesh.TriangleMesh
import scalismo.numerics.FixedPointsUniformMeshSampler3D
import scalismo.registration.Transformation
import scalismo.statisticalmodel.asm.{ActiveShapeModel, GaussianGradientImagePreprocessor, NormalDirectionFeatureExtractor}
import scalismo.utils.Benchmark
import spire.math.{UByte, UInt, ULong, UShort}

import scala.reflect.ClassTag
import scala.reflect.runtime.universe.TypeTag

object ASMBuild {
  def main(args: Array[String]): Unit = {
    if (args.length < 8) {
      usage()
      System.exit(1)
    }

    val ssmFile = new File(args(0))
    val imagesDir = new File(args(1))
    val meshesDir = new File(args(2))
    val outputFile = new File(args(3))
    val numProfilePoints = args(4).toInt
    val profileSize = args(5).toInt
    val profileSpacing = args(6).toFloat
    val gaussSigma = args(7).toFloat

    Benchmark.benchmark({
      scalismo.initialize()
      val ssm = StatismoIO.readStatismoMeshModel(ssmFile).get

      val preprocessor = GaussianGradientImagePreprocessor(gaussSigma)
      val featureExtractor = NormalDirectionFeatureExtractor(profileSize, profileSpacing)

      val sampler = (m: TriangleMesh) => new FixedPointsUniformMeshSampler3D(m, numProfilePoints, seed = 42)

      val trainingData = makeTrainingData(imagesDir, meshesDir, ssm.referenceMesh)

      val asm = ActiveShapeModel.trainModel(ssm, trainingData, preprocessor, featureExtractor, sampler)
      ActiveShapeModelIO.writeActiveShapeModel(asm, outputFile).get
    }, "total duration")
  }

  def checkmemX(): Unit = {
    System.gc()
    System.gc()
    System.gc()
    println("Memory used: " + ((Runtime.getRuntime.totalMemory() - Runtime.getRuntime.freeMemory()) / (1024 * 1024)) + " MB")
  }

  def usage(): Unit = {
    println("Usage: java -jar asmbuild.jar ssmFile imagesDir meshesDir outputFile numProfilePoints profileSize profileSpacing gaussSigma")
    println()
    println("  ssmFile:          existing statistical shape model that the ASM will be based on.")
    println("  imagesDir:        directory with images (volumes). .nii and .vtk are supported.")
    println("  meshesDir:        directory with meshes (surfaces). .vtk, .h5 and .stl are supported.")
    println("  outputFile:       the file the ASM will be written to.")
    println("  numProfilePoints: the number of profile points (\"landmarks\") to sample. They will be uniformly sampled on the reference mesh.")
    println("  profileSize:      the size of every profile (number of elements). Preferrably use odd values, not even ones.")
    println("  profileSpacing:   the spacing between the profile elements (in mm). The total length of each profile will be profileSpacing * profileSize.")
    println("  gaussSigma:       standard deviation to use for gauss smoothing when preprocessing images. Set to 0 to disable smoothing.")
    println()
    println("NOTES:")
    println("  - images and meshes must be aligned with the model.")
    println("  - meshes must be in correspondence.")
    println("  - all image and mesh files directly in the given directories are evaluated (no recursion).")
    println("    Correspondence between image and mesh files is determined by matching the files' base names.")
  }

  def makeTrainingData(imagesDir: File, meshesDir: File, reference: TriangleMesh) = {
    val images = imagesDir.listFiles(new FilenameFilter {
      override def accept(dir: File, name: String): Boolean = name.endsWith(".vtk") || name.endsWith(".nii")
    })

    val meshes = meshesDir.listFiles(new FilenameFilter {
      override def accept(dir: File, name: String): Boolean = name.endsWith(".vtk") || name.endsWith(".h5") || name.endsWith(".stl")
    })

    val iMap = images.map(f => (basename(f), f)).toMap
    val mMap = meshes.map(f => (basename(f), f)).toMap

    val imgAndMeshFiles = iMap.filterKeys(n => mMap.contains(n)).map { case (k, v) => (v, mMap(k)) }.toIndexedSeq.sortBy(t => basename(t._1))

    def trainingDatum(files: (File, File)): (DiscreteScalarImage[_3D, Float], Transformation[_3D]) = {
      val (imageFile, meshFile) = files
      println(s"$imageFile, $meshFile")

      val image = loadImage(imageFile)
      val mesh = MeshIO.readMesh(meshFile).get
      val transform = surfaceToTransform(reference, mesh)

      (image, transform)
    }

    println(s"Building Active Shape Model from ${imgAndMeshFiles.length} training data...")

    imgAndMeshFiles.iterator map trainingDatum
  }

  def loadImage(file: File): DiscreteScalarImage[_3D, Float] = {

    def loadAs[T: Scalar : TypeTag : ClassTag]: DiscreteScalarImage[_3D, T] = {
      ImageIO.read3DScalarImage[T](file).get
    }

    val scalar = ScalarType.ofFile(file).get
    scalar match {
      case ScalarType.Byte => loadAs[Byte].map(_.toFloat)
      case ScalarType.Short => loadAs[Short].map(_.toFloat)
      case ScalarType.Int => loadAs[Int].map(_.toFloat)
      case ScalarType.Long => loadAs[Long].map(_.toFloat)
      case ScalarType.Float => loadAs[Float]
      case ScalarType.Double => loadAs[Double].map(_.toFloat)
      case ScalarType.UByte => loadAs[UByte].map(_.toFloat)
      case ScalarType.UShort => loadAs[UShort].map(_.toFloat)
      case ScalarType.UInt => loadAs[UInt].map(_.toFloat)
      case ScalarType.ULong => loadAs[ULong].map(_.toFloat)
      case _ => throw new Exception()
    }
  }

  def surfaceToTransform(ref: TriangleMesh, target: TriangleMesh): Transformation[_3D] = {
    require(ref.numberOfPoints == target.numberOfPoints)
    new Transformation[_3D] {
      override def domain = RealSpace[_3D]

      override val f: (Point[_3D]) => Point[_3D] = { pt =>
        val (_, ptId) = ref.findClosestPoint(pt)
        val tgtPt = target.points(ptId)
        tgtPt
      }
    }
  }


  def basename(file: File): String = {
    val name = file.getName
    val dot = name.lastIndexOf(".")
    name.substring(0, dot)
  }
}
